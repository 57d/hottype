# HotType
A project to emulate fast typing of often used text or special symbols
You can combine this project with, for example, KDE's shortcuts to bind typing to key combinations

## Usage
1. Download hot_type.py
2. Invoke `hot_type.py --type=<type> --data=<data>`

`<type>`:
- "text": emulate typing data. However, special symbols(emojis, rare alphabets) may not be typed correctly, try using "clip" or "paste" instead
- "clip": copy data to clipboard
- "paste": copy data to clipboard, emulate "Ctrl-V", return previous clipboard data(if you have "hello world" copied and "to-paste" as data, "to-paste" will be pasted, but if you try to paste again by pressing "Ctrl-V", "hello world" will be pasted)

`<data>`: what should be typed/copied/pasted
