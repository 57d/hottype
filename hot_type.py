#!/usr/bin/python
import click
import time
import pyperclip
from pynput import keyboard, mouse

@click.command()
@click.option("--type", default="text", help="Type of hotkey.")
@click.option("--data", default="-", help="Data of hotkey.")

def click(type, data):
    if type == "text":
        controller.tap(data)
    if type == "clip":
        clip = pyperclip.paste()
        pyperclip.copy(data)
    if type == "paste":
        clip = pyperclip.paste()
        pyperclip.copy(data)
        controller.press(keyboard.Key.ctrl)
        controller.press("v")
        controller.release(keyboard.Key.ctrl)
        controller.release("v")
        time.sleep(1)
        pyperclip.copy(clip)


controller = keyboard.Controller()

click()
